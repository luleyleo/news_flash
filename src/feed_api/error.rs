use crate::error::FeedParserError;

use super::portal::PortalError;
use semver::Version;
use thiserror::Error;
use tokio::sync::AcquireError;

#[derive(Error, Debug)]
pub enum FeedApiError {
    #[error("Error reading config file")]
    Config(#[from] serde_json::error::Error),
    #[error("Error parsing Url")]
    Url(#[from] url::ParseError),
    #[error("Error parsing Json file")]
    Json { source: serde_json::Error, json: String },
    #[error("Network Error")]
    Network(#[from] reqwest::Error),
    #[error("Error during portal callback")]
    Portal(#[from] PortalError),
    #[error("IO Error")]
    IO(#[from] std::io::Error),
    #[error("API Error: {message}")]
    Api { message: String },
    #[error("API Limit Reached")]
    ApiLimit,
    #[error("Auth required/failed")]
    Auth,
    #[error("Not logged in")]
    Login,
    #[error("Error loading resource")]
    Resource,
    #[error("Feature unsupported by implementation")]
    Unsupported,
    #[error("Error parsing feed url")]
    ParseFeed(#[from] FeedParserError),
    #[error("Error de/encrypting data")]
    Encryption,
    #[error("Couldn't acquire download permit from semaphore")]
    Semaphore(#[from] AcquireError),
    #[error("Version not supported")]
    UnsupportedVersion { min_supported: Version, found: Option<Version> },
    #[error("Unknown Error")]
    Unknown,
}
