use super::{Category, CategoryMapping, Enclosure, FatArticle, Feed, FeedMapping, Headline, Tagging};

pub struct StreamConversionResult {
    pub articles: Vec<FatArticle>,
    pub headlines: Vec<Headline>,
    pub taggings: Vec<Tagging>,
    pub enclosures: Vec<Enclosure>,
}

impl Default for StreamConversionResult {
    fn default() -> Self {
        Self::new()
    }
}

impl StreamConversionResult {
    pub fn new() -> Self {
        Self {
            articles: Vec::new(),
            headlines: Vec::new(),
            taggings: Vec::new(),
            enclosures: Vec::new(),
        }
    }

    pub fn add(&mut self, mut other: Self) {
        self.articles.append(&mut other.articles);
        self.headlines.append(&mut other.headlines);
        self.taggings.append(&mut other.taggings);
        self.enclosures.append(&mut other.enclosures);
    }
}

pub struct FeedConversionResult {
    pub feeds: Vec<Feed>,
    pub feed_mappings: Vec<FeedMapping>,
    pub categories: Vec<Category>,
    pub category_mappings: Vec<CategoryMapping>,
}
