use std::hash::{Hash, Hasher};

use super::{ArticleID, Url};
use crate::schema::images;

#[derive(Identifiable, Clone, Insertable, Queryable, Eq, Debug)]
#[diesel(primary_key(image_url))]
#[diesel(table_name = images)]
pub struct Image {
    pub image_url: Url,
    pub article_id: ArticleID,
    pub file_path: String,
}

impl Hash for Image {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.image_url.hash(state);
    }
}

impl PartialEq for Image {
    fn eq(&self, other: &Image) -> bool {
        self.image_url == other.image_url
    }
}
