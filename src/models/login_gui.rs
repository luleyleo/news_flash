use std::fmt::Debug;

use super::ApiSecret;
use crate::models::Url;

type LoginUrlFn = dyn Fn(Option<&ApiSecret>) -> Option<Url>;

#[derive(Debug)]
pub enum LoginGUI {
    Direct(DirectLoginGUI),
    OAuth(Box<OAuthLoginGUI>),
    None,
}

pub struct OAuthLoginGUI {
    pub login_website: Box<LoginUrlFn>,
    pub catch_redirect: Option<String>,
    pub custom_api_secret: bool,
    pub custom_api_secret_url: Option<Url>,
    pub embeded_api_secret: bool,
    pub create_secret_url: Option<Url>,
}

impl Debug for OAuthLoginGUI {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("OAuthLoginGUI")
            .field("catch_redirect", &self.catch_redirect)
            .field("custom_api_secret", &self.custom_api_secret)
            .field("custom_api_secret_url", &self.custom_api_secret_url)
            .field("embeded_api_secret", &self.embeded_api_secret)
            .field("create_secret_url", &self.create_secret_url)
            .finish()
    }
}

#[derive(Clone, Debug)]
pub struct DirectLoginGUI {
    pub url: bool,
    pub support_token_login: bool,
    pub http_auth: bool,
}

impl Default for DirectLoginGUI {
    fn default() -> Self {
        Self {
            url: true,
            support_token_login: false,
            http_auth: false,
        }
    }
}
