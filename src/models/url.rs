use diesel::backend::Backend;
use diesel::deserialize::{self, FromSql};
use diesel::serialize::{self, IsNull, Output, ToSql};
use diesel::sql_types::Text;
use diesel::sqlite::Sqlite;
use log::error;
use std::fmt;
use std::ops::{Deref, DerefMut};
use std::str;

#[derive(Debug, PartialEq, Eq, Hash, Clone, AsExpression, FromSqlRow)]
#[diesel(sql_type = Text)]
pub struct Url(url::Url);

impl Url {
    pub fn new(url: url::Url) -> Self {
        Url(url)
    }

    pub fn parse(url: &str) -> Result<Self, url::ParseError> {
        let url = url::Url::parse(url).map_err(move |err| {
            error!("Failed parsing url: {}", url);
            err
        })?;
        Ok(Url::new(url))
    }

    pub fn base(&self) -> Result<Self, url::ParseError> {
        let mut clone = self.clone();
        match clone.path_segments_mut() {
            Ok(mut path) => {
                path.clear();
            }
            Err(_) => {
                return Err(url::ParseError::RelativeUrlWithCannotBeABaseBase);
            }
        }
        clone.set_query(None);
        clone.set_fragment(None);
        Ok(clone)
    }
}

impl Deref for Url {
    type Target = url::Url;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Url {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl fmt::Display for Url {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl FromSql<Text, Sqlite> for Url {
    fn from_sql(bytes: <Sqlite as Backend>::RawValue<'_>) -> deserialize::Result<Self> {
        let bytes = <Vec<u8>>::from_sql(bytes)?;
        let string = str::from_utf8(&bytes)?;
        let url = url::Url::parse(string)?;
        Ok(Url::new(url))
    }
}

impl ToSql<Text, Sqlite> for Url {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Sqlite>) -> serialize::Result {
        out.set_value(self.as_str());
        Ok(IsNull::No)
    }
}

#[cfg(test)]
mod tests {
    use super::Url;

    #[test]
    fn base_url() {
        let url = Url::parse("https://kryogenix.org/images/me/hackergotchi-simpler.png").unwrap();
        let base = url.base().unwrap();
        assert_eq!("https://kryogenix.org/", base.to_string())
    }
}
