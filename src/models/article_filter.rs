use crate::models::{ArticleID, ArticleOrder, CategoryID, FeedID, Marked, Read, TagID};
use chrono::{DateTime, Utc};

#[derive(Clone, Debug, Default)]
pub struct ArticleFilter {
    pub limit: Option<i64>,
    pub offset: Option<i64>,
    pub order: Option<ArticleOrder>,
    pub unread: Option<Read>,
    pub marked: Option<Marked>,
    pub feeds: Option<Vec<FeedID>>,
    pub feed_blacklist: Option<Vec<FeedID>>,
    pub categories: Option<Vec<CategoryID>>,
    pub category_blacklist: Option<Vec<CategoryID>>,
    pub tags: Option<Vec<TagID>>,
    pub ids: Option<Vec<ArticleID>>,
    pub newer_than: Option<DateTime<Utc>>,
    pub older_than: Option<DateTime<Utc>>,
    pub synced_before: Option<DateTime<Utc>>,
    pub synced_after: Option<DateTime<Utc>>,
    pub search_term: Option<String>,
}

impl ArticleFilter {
    pub fn ids(article_ids: Vec<ArticleID>) -> Self {
        Self {
            ids: Some(article_ids),
            ..Self::default()
        }
    }

    pub fn read_ids(article_ids: Vec<ArticleID>, read: Read) -> Self {
        Self {
            unread: Some(read),
            ids: Some(article_ids),
            ..Self::default()
        }
    }

    pub fn marked_ids(article_ids: Vec<ArticleID>, marked: Marked) -> Self {
        Self {
            marked: Some(marked),
            ids: Some(article_ids),
            ..Self::default()
        }
    }

    pub fn feed_unread(feed_id: &FeedID) -> Self {
        Self {
            unread: Some(Read::Unread),
            feeds: Some([feed_id.clone()].into()),
            ..Self::default()
        }
    }

    pub fn category_unread(category_id: &CategoryID) -> Self {
        Self {
            unread: Some(Read::Unread),
            categories: Some([category_id.clone()].into()),
            ..Self::default()
        }
    }

    pub fn tag_unread(tag_id: &TagID) -> Self {
        Self {
            unread: Some(Read::Unread),
            tags: Some([tag_id.clone()].into()),
            ..Self::default()
        }
    }

    pub fn all_unread() -> Self {
        Self {
            unread: Some(Read::Unread),
            ..Self::default()
        }
    }

    pub fn all_marked() -> Self {
        Self {
            marked: Some(Marked::Marked),
            ..Self::default()
        }
    }
}
