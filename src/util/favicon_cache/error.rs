use crate::database::DatabaseError;
use thiserror::Error;
use tokio::sync::AcquireError;

#[derive(Error, Debug)]
pub enum FavIconError {
    #[error("Error performing DB operation")]
    DB(#[from] DatabaseError),
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("Error parsing HTML")]
    Html,
    #[error("No Feed")]
    NoFeed,
    #[error("Error resizing icon")]
    Resize,
    #[error("Couldn't acquire download permit from semaphore")]
    Semaphore(#[from] AcquireError),
}
