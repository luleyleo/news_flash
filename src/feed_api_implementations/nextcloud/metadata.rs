use super::config::AccountConfig;
use super::Nextcloud;
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiError, FeedApiResult, Portal};
use crate::models::{
    ApiSecret, DirectLoginGUI, LoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, Url, VectorIcon,
};
use nextcloud_news_api::NextcloudNewsApi;
use rust_embed::RustEmbed;
use std::path::Path;
use std::str;
use std::sync::Arc;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/nextcloud/icons"]
struct NextcloudResources;

pub struct NextcloudMetadata;

impl NextcloudMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("nextcloud")
    }
}

impl ApiMetadata for NextcloudMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = NextcloudResources::get("feed-service-nextcloud.svg").ok_or(FeedApiError::Resource)?;
        let icon = VectorIcon {
            data: icon_data.data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = NextcloudResources::get("feed-service-nextcloud-symbolic.svg").ok_or(FeedApiError::Resource)?;
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.data.to_vec(),
            width: 48,
            height: 24,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let login_gui = LoginGUI::Direct(DirectLoginGUI::default());

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("Nextcloud News"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            website: match Url::parse("https://apps.nextcloud.com/apps/news") {
                Ok(website) => Some(website),
                Err(_) => None,
            },
            service_type: ServiceType::Remote { self_hosted: true },
            license_type: ServiceLicense::GPLv3,
            service_price: ServicePrice::Free,
            login_gui,
        })
    }

    fn get_instance(&self, path: &Path, portal: Box<dyn Portal>, _user_api_secret: Option<ApiSecret>) -> FeedApiResult<Box<dyn FeedApi>> {
        let account_config = AccountConfig::load(path)?;
        let mut api: Option<NextcloudNewsApi> = None;

        if let Some(url) = account_config.get_url() {
            if let Ok(url) = Url::parse(&url) {
                if let Some(username) = account_config.get_user_name() {
                    if let Some(password) = account_config.get_password() {
                        api = Some(NextcloudNewsApi::new(&url, username, password)?);
                    }
                }
            }
        }

        let logged_in = api.is_some();

        let nextcloud = Nextcloud {
            api,
            portal: Arc::new(portal),
            logged_in,
            config: account_config,
        };
        let nextcloud = Box::new(nextcloud);
        Ok(nextcloud)
    }
}
