DROP TRIGGER on_delete_feed_trigger;
DROP TRIGGER on_delete_category_trigger;
DROP TRIGGER on_delete_article_trigger;
DROP TRIGGER on_delete_tag_trigger;


CREATE TRIGGER on_delete_feed_trigger
	AFTER DELETE ON feeds
	BEGIN
		DELETE FROM feed_mapping WHERE feed_mapping.feed_id=OLD.feed_id;
		DELETE FROM articles WHERE articles.feed_id=OLD.feed_id;
		DELETE FROM fav_icons WHERE fav_icons.feed_id=OLD.feed_id;
	END;

CREATE TRIGGER on_delete_category_trigger
	AFTER DELETE ON categories
	BEGIN
		DELETE FROM feed_mapping WHERE feed_mapping.category_id=OLD.category_id;
	END;

CREATE TRIGGER on_delete_tag_trigger
	AFTER DELETE ON tags
	BEGIN
		DELETE FROM taggings WHERE taggings.tag_id=OLD.tag_id;
	END;